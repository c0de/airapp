package controllers

import javax.inject._

import controllers.CountryQueryForm._
import play.api.data.Form
import play.api.db.Database
import play.api.i18n._
import play.api.mvc._

/**
  * This controller creates an `Action` to handle HTTP requests to the application's home page.
  */
@Singleton
class HomeController @Inject()(cc: ControllerComponents, db: Database) extends AbstractController(cc) with I18nSupport {
  /**
    * Home page
    */
  def index = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index(
      form, // form to get country input
      routes.HomeController.countryQueryHandler(), // handle the request and redirect to the display page
      routes.HomeController.displayReportQueryResult())) // url to show report query result
  }

  /**
    * Get report and call the view
    * TODO send as Json
    */
  def displayReportQueryResult = Action { implicit request =>
    import database.Report
    import play.api.libs.json._
    import database.ReportValueWrites._
    val report = new Report(db).reportValue
    Ok(Json.toJson(report)) // TODO propose raw, pretty print, download
  }

  /**
    * Get result of the query and call the view
    */
  def countryQueryHandler = Action { implicit request: Request[AnyContent] =>
    val errorFunction = { formWithErrors: Form[CountryData] =>
      // This is the bad case, where the form had validation errors.
      // Let's show the user the form again, with the errors highlighted.
      // Note how we pass the form with errors to the template.
      BadRequest(views.html.error())
    }

    // This is the good case, where the form was successfully parsed as a CountryQuery.
    val successFunction = { data: CountryData =>
      import database.Query
      val res = new Query(db).query(data.input)
      Ok(views.html.displayQueryResult(data.input, res))
    }

    val formValidationResult = form.bindFromRequest
    formValidationResult.fold(errorFunction, successFunction)
  }
}