package controllers

import play.api.data.Forms._
import play.api.data.Form

object CountryQueryForm {
  case class CountryData(input: String)

  val form = Form(
    mapping(
      "input" -> nonEmptyText
    )(CountryData.apply)(CountryData.unapply)
  )
}