package database

import anorm.SqlParser._
import anorm._
import models.CountryQueryResult
import play.api.db._

class Query(db: Database) {
  private val parserQuery = {
      get[String](s"${Tables.countries}.name") ~
      get[String](s"${Tables.airports}.name") map {
      case nameC ~ nameA => CountryQueryResult(nameC, nameA, "")
    }
  }

  /**
    * Return the list airports of the corresponding country
    * CAUTION: Runways missing to avoid excessive slow down
    * @param countryInput code or partial name of a country
    * @return
    */
  def query(countryInput: String): List[CountryQueryResult] = {
    db.withConnection { implicit connection =>
      SQL(
        s"""SELECT
           | ${Tables.countries}.name,
           | ${Tables.airports}.name
           |FROM ${Tables.countries}
           |
           |INNER JOIN ${Tables.airports} ON ${Tables.countries}.code = ${Tables.airports}.iso_country
           |
           |WHERE LOWER(${Tables.countries}.name) LIKE LOWER('%$countryInput%')
           |OR ${Tables.countries}.code = '$countryInput'
           |ORDER BY ${Tables.countries}.name ASC
        """.stripMargin
      ).as(parserQuery.*)
    }
  }

  // TODO
  // The join on RUNWAYS slows down excessively the app
  // Materialized view were not accepted by H2, even in Oracle mode.
  /*
    private val parserQuery = {
      get[String](s"${Tables.countries}.name") ~
      get[String](s"${Tables.airports}.name") ~
      get[String](s"${Tables.runways}.id") map {
      case nameC ~ nameA ~ idR => CountryQueryResult(nameC, nameA, idR)
    }
  }

  def query(countryInput: String): List[CountryQueryResult] = {
    db.withConnection { implicit connection =>
      SQL(
        s"""SELECT
          | ${Tables.countries}.name,
          | ${Tables.airports}.name,
          | ${Tables.runways}.id
          |FROM ${Tables.countries}
          |
          |INNER JOIN ${Tables.airports} ON ${Tables.countries}.code = ${Tables.airports}.iso_country
          |INNER JOIN ${Tables.runways} ON ${Tables.airports}.id = ${Tables.runways}.airport_ref
          |
          |WHERE LOWER(${Tables.countries}.name) LIKE LOWER('%$countryInput%')
          |OR ${Tables.countries}.code = '${countryInput}'
          |ORDER BY ${Tables.countries}.name ASC
        """.stripMargin
      ).as(parserQuery.*)
    }
  }
  */
}

