package database

import anorm.SqlParser.get
import anorm.{SQL, ~}
import play.api.db.Database

case class CountryAirportsCount(country: String, count: Int)
case class HighestLowestTopTen(highest: List[CountryAirportsCount], lowest: List[CountryAirportsCount])
case class ReportValue(topTenCountries: HighestLowestTopTen, typeRunways: List[String], topTenRunwaysId: List[String])

object ReportValueWrites {
  import play.api.libs.json._

  implicit val cacWrites = Json.writes[CountryAirportsCount]
  implicit val hlttWrites = Json.writes[HighestLowestTopTen]
  implicit val rvWrites = Json.writes[ReportValue]
}

class Report(db: Database) {
  lazy val reportValue = ReportValue(
    getCountAirportPerCountry,
    Nil,
    getMostCommonRunwaysId
  )

  private val parserReportCA = {
    get[String](1) ~ get[Int](2) map {
      case name ~ count => CountryAirportsCount(name, count)
    }
  }

  private def getCA: List[CountryAirportsCount] = {
    db.withConnection { implicit connection =>
      SQL(
        s"""SELECT ${Tables.countries}.name, COUNT(${Tables.airports}.name)
           |FROM ${Tables.countries}
           |INNER JOIN ${Tables.airports} ON ${Tables.countries}.code = ${Tables.airports}.iso_country
           |GROUP BY ${Tables.countries}.name
        """.stripMargin
      ).as(parserReportCA.*)
    }
  }

  /**
    * Return 10 countries with highest, lowest number of airports
    * @return (highest, lowest)
    */
  private def getCountAirportPerCountry = {
    val sorted = getCA.sortBy(- _.count) // descending sort
    HighestLowestTopTen(sorted.take(10), sorted.drop(sorted.size - 10))
  }

  private def getRunwaysId = {
    db.withConnection { implicit connection =>
      SQL(
        s"""SELECT ${Tables.runways}.le_ident
           |FROM ${Tables.runways}
           |GROUP BY ${Tables.runways}.le_ident
           |ORDER BY ${Tables.runways}.le_ident ASC LIMIT 10
        """.stripMargin
      ).as(get[Option[String]](1).*)
    }
  }

  /**
    * Return 10 most common runways id "le_ident"
    * @return the ID or "" if null
    */
  private def getMostCommonRunwaysId: List[String] = {
    getRunwaysId.map{
      case Some(v) => v
      case None => ""
    }
  }
}
