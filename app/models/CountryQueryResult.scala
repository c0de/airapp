package models

case class CountryQueryResult(nameCountry: String, nameAirport: String, idRunway: String) {
  override def toString: String = nameCountry + ", " + nameAirport + ", " + idRunway
}
